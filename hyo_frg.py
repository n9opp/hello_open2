from reportlab.pdfgen import canvas
from reportlab.lib import pagesizes
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase import cidfonts
from reportlab.pdfbase import ttfonts


## font指定
# VeraIt
pdfmetrics.registerFont(ttfonts.TTFont('VeraIt', 'VeraIt.ttf'))
# HKG
HKG='HeiseiKakuGo-W5'
pdfmetrics.registerFont(cidfonts.UnicodeCIDFont(HKG))
# HM
HM='HeiseiMin-W3'
pdfmetrics.registerFont(cidfonts.UnicodeCIDFont(HM))

## 用紙指定
p=canvas.Canvas('./hyo_frg.pdf',bottomup=False,pagesize=pagesizes.portrait(pagesizes.A4))

base_font_name=HKG
base_font_size=5      #ミリ指定


def base_font():     #font初期設定
  p.setFont(base_font_name,m2p(base_font_size))

def m2p(s):          #ミリをポイントに
   pt=s/0.35277
   return pt   

def p_str(xy,str,**size_font):    #文字列印字
   px=m2p(xy[0])
   py=m2p(xy[1])
   #print(size_font)
   if size_font=={}:
      p.drawString(px,py,str)   #
   else:
      s=base_font_size
      f=base_font_name
      if 'size' in size_font.keys():
         s=size_font['size']
      if 'font' in size_font.keys():
         f=size_font['font']
      p.setFont(f,m2p(s))
      p.drawString(px,py,str)   #
      base_font()
   

def p_line(xy,xy2,width,type):   #線印字  (type未)
   px=m2p(xy[0])
   py=m2p(xy[1])
   px2=m2p(xy2[0])
   py2=m2p(xy2[1])
   pw=m2p(width)
   p.setLineWidth(pw)
   p.line(px,py,px2,py2)
   


p_str([10,20],"あAbc",size=10,font=HM)
p_str([30,30],"あAbc")
p_str([30,50],"Abc",size=20)
p_str([30,60],"AbcItalic",font='VeraIt')

p_line([30,30],[100,100],1,"")

p.save()

