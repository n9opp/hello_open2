from reportlab.pdfgen import canvas
from reportlab.lib import pagesizes
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase import cidfonts
from reportlab.pdfbase import ttfonts
import textwrap
from collections import Counter

## font指定
# VeraIt
pdfmetrics.registerFont(ttfonts.TTFont('VeraIt', 'VeraIt.ttf'))
# HKG
HKG='HeiseiKakuGo-W5'
pdfmetrics.registerFont(cidfonts.UnicodeCIDFont(HKG))
# HM
HM='HeiseiMin-W3'
pdfmetrics.registerFont(cidfonts.UnicodeCIDFont(HM))

testfont='BRDB4RT0.TTF'
pdfmetrics.registerFont(ttfonts.TTFont('tes', testfont))

base_font_name=HM
base_font_size=5      #ミリ指定


def m2p(s):          #ミリをポイントに
   pt=s/0.35277
   return pt   

def base_font():     #font初期設定
  p.setFont(base_font_name,m2p(base_font_size))

def p_str(xy,str,**size_font):    #文字列印字
   px=m2p(xy[0])
   py=m2p(xy[1])
   #print(size_font)
   if size_font=={}:
      p.drawString(px,py,str)   #
   else:
      s=base_font_size
      f=base_font_name
      if 'size' in size_font.keys():
         s=size_font['size']
      if 'font' in size_font.keys():
         f=size_font['font']
      p.setFont(f,m2p(s))
      p.drawString(px,py,str)   #
      base_font()



### 文章印刷
## 用紙指定
p=canvas.Canvas('./frg_print.pdf',bottomup=False,pagesize=pagesizes.portrait(pagesizes.A4))

base_font()
n=0
npy=20
f=open('frg_p.txt',encoding='utf-8')
t=f.readline()
while t:
   tt=textwrap.fill(t, width=90)
  # print(tt)
  # print("---")
   n=n+1
  # if n==40:
  #    break
   tobj = p.beginText(m2p(10),m2p(npy))
   tobj.textLines(tt)
   p.drawText(tobj)
   #
   npy=npy+tt.count('\n')*6+8
   if npy>285:
      p.showPage()
      npy=20
      base_font()
   t = f.readline()
f.close
p.save()


### 集計
## 用紙指定
p=canvas.Canvas('./frg_syukei.pdf',bottomup=False,pagesize=pagesizes.portrait(pagesizes.A4))

f=open('frg_p.txt',encoding='utf-8')
t=f.readlines()
f.close

gyosu=len(t)

tt=' '.join(t)
tango=tt.split(' ')
tangosu=len(tango)

tangos=[]
mojisu=0
for w in tango:
   w=w.strip()
   w=w.strip('.')
   w=w.strip(',')
   #print(w+"<")
   mojisu=mojisu+len(w)
   tangos.append(w)

#print(mojisu)

base_font()

cnt=Counter(tangos)
x0=10
y0=20
x=x0
y=y0
for word,count in cnt.most_common():
    pw="%s  %d" % (word, count)
    p_str([x,y],pw)
    x=x+30
    if x>160:
       y=y+8
       x=x0
    if y>290:
       p.showPage()
       y=y0  

x=x0
y=y+15
p_str([x,y],"行数 %d" % gyosu,font=HM)
y=y+10
p_str([x,y],"単語数 %d" % tangosu)
y=y+10
p_str([x,y],"文字数 %d" % mojisu)


p.save()

