from http.server import HTTPServer, CGIHTTPRequestHandler

class Handler(CGIHTTPRequestHandler):
    cgi_directories = ['/cgi-bin', '/htbin', '/']

PORT = 8080
httpd = HTTPServer(('localhost', PORT), Handler)
httpd.serve_forever()
