import urllib.request
import re

#url="http://hayabusa.open2ch.net/test/read.cgi/livejupiter/1495822674/"
url="http://hayabusa.open2ch.net/test/read.cgi/livejupiter/1437376105/"

def main():
   # test_read(url)
   # local_save(url)       #ローカルへファイル名'w'で保存
   # local_save_dat(url)   #datを'w_dat'で保存
   
   n=0
   print(url)
   d=local_read('w')
   title(d)
   t=res(d)
   print()
   ff=open('w_put','w')
   for w in t:
      w=re.sub('\n|\r','',w)
    #  print(w)
      w1=re.search('res=(.*?)>',w)     ## No
      w1=w1.group(1)

      wa=re.search('：(.*?)<span',w)   ## wa:名前,sage,日付
      wa=wa.group(1)

      w2=re.search('^(.*?)：',wa)      ## wb:名前,sage
      wb=w2.group(1)
      w2=re.search('<b>(.*?)</b>',wb)  ## 名前
      w2=w2.group(1)
     
      w2b=re.search('mailto:(.*?)>',wb)  ## sage
      if w2b is None:
         w2b=''
      else:
         w2b=w2b.group(1)

      w3=re.search('：(.*?)$',wa)      ## 日付
      w3=w3.group(1)

      w4=re.search('>ID:(.*?)<',w)     ## ID
      w4=w4.group(1)

      w5=re.search('class="id\w.+?">(.*?)<ares',w)     ## コメント
      w5=w5.group(1)
      #
      w5=re.sub('<a rel=.*?>',"",w5)   ##(削除)
      w5=re.sub('</a>',"",w5)          ##(削除)
      w5=re.sub('>>',"&gt;&gt;",w5)    ##(置換)
    #  w5=re.sub('<br>$',"",w5)        ##(最後<br>削除)
      w5=w5.rstrip('<br> ')
      
      www='{0}<>{1}<>{2}ID:{3}<> {4} <>'.format(w2,w2b,w3,w4,w5)
      print(www)
      print()
      ff.write(www)
      ff.write('\r')
      n=n+1
     # if n==20: break
   ff.close()
   
   
   
def test_read(w):                      ### urlテスト読取
   rp=urllib.request.urlopen(w)
   data_r=rp.read()
   print(data_r)
   data_d=data_r.decode('utf-8')
   print(data_d)
   
def local_save(w):                     ### url読取,ローカル保存   
   urllib.request.urlretrieve(w,"w")   

def local_save_dat(w):                 ### dat,ローカル保存   
   p=w.split('/')
   ww=[p[0],p[1],p[2],p[5],'dat',p[6]]
   wf='/'.join(ww)+'.dat'
   urllib.request.urlretrieve(wf,"w_dat")   

def local_read(w):                     ### ローカルデータ読取
   f=open(w,encoding='utf-8')
   t=f.read()
   f.close()
   return t

def title(w):
   t=re.search('<title>(.*)</title>',w)
   print(t.group(1))

def res(w):
   t=re.findall(r'<dt res.*?</dt>',w,flags=re.DOTALL)
   return t

if __name__ == "__main__":
   main()

